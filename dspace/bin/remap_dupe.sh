#!/bin/bash
#
# This remaps a single deduplicated item.
# Arguments are the handle of the deleted item and the 
# canonical handle.

if [ "$#" -ne 2 ]
then
  echo Usage: bash remap_dupe.sh deleted_handle remaining_handle
  exit 1
fi

# Get needed database variables from local.cfg
DB_PASSWORD=$(sed -n "/db.password = /,/^$/p" /dspace/config/local.cfg | sed -n "s/db.password = //p")
DB_URL=$(sed -n "/db.url = /,/^$/p" /dspace/config/local.cfg | sed -n "s/db.url = jdbc:postgresql:\/\///p")
DB_HOST=$(echo $DB_URL | awk -F ':' '{print $1;}' )
DB_PORT=$(echo $DB_URL | awk -F ':' '{print $2;}' | awk -F '/' '{print $1;}')
DB_NAME=$(echo $DB_URL | awk -F ':' '{print $2;}' | awk -F '/' '{print $2;}')
DB_USER=$(sed -n "/db.username = /,/^$/p" /dspace/config/local.cfg | sed -n "s/db.username = //p")

echo using database at $DB_URL

# Execute SQL from psql using database config
echo Mapping handle "$1" to handle "$2"
PGPASSWORD=$DB_PASSWORD psql -h $DB_HOST -p $DB_PORT -U $DB_USER -d $DB_NAME -c "UPDATE handle SET resource_id = ( SELECT resource_id FROM handle WHERE handle='$2') WHERE handle='$1'"