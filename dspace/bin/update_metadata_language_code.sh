#!/bin/bash

# Get needed database variables from local.cfg
DB_PASSWORD=$(sed -n "/db.password = /,/^$/p" /dspace/config/local.cfg | sed -n "s/db.password = //p")
DB_URL=$(sed -n "/db.url = /,/^$/p" /dspace/config/local.cfg | sed -n "s/db.url = jdbc:postgresql:\/\///p")
DB_HOST=$(echo $DB_URL | awk -F ':' '{print $1;}' )
DB_PORT=$(echo $DB_URL | awk -F ':' '{print $2;}' | awk -F '/' '{print $1;}')
DB_NAME=$(echo $DB_URL | awk -F ':' '{print $2;}' | awk -F '/' '{print $2;}')
DB_USER=$(sed -n "/db.username = /,/^$/p" /dspace/config/local.cfg | sed -n "s/db.username = //p")

echo using database at $DB_URL

# Execute SQL from psql using database config
# Due to differences in database configuration for LDEs and
# production instances, there are separate commands if the database
# is local or remote.
#if [  "$DB_HOST" = "localhost" ]; then
#  echo Using command for local database
#  psql -U $DB_USER -d $DB_NAME -c "UPDATE metadatavalue SET text_lang='en' FROM item WHERE metadatavalue.dspace_object_id=item.uuid AND (text_lang IN ('', 'en_US', 'en US', '*') OR text_lang IS NULL)"
#else
  echo Using command for remote database
  PGPASSWORD=$DB_PASSWORD psql -h $DB_HOST -p $DB_PORT -U $DB_USER -d $DB_NAME -c "UPDATE metadatavalue SET text_lang='en' FROM item WHERE metadatavalue.dspace_object_id=item.uuid AND (text_lang IN ('', 'en_US', 'en US', '*') OR text_lang IS NULL)"
#  fi