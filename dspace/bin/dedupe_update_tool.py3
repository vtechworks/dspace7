"""
Update the DSpace database so that items we've designated as duplicates
are remapped to the canonical location. Links to the deleted item via
the handle point to the new location after running the command.

This command expects that dedupe.map is found in the current directory.

dedupe.map is a text file with lines in this format:
/handle/10919/104492 /handle/10919/101437;

The first argument is the handle of the deleted item; the
second argument is the canonical handle of the merged item.
"""

import subprocess
import sys

# Mappings are in the file dedupe.map
with open('dedupe.map', 'r', encoding="utf-8") as dedupe_map:
    mappings = dedupe_map.readlines()

# For each mapping, call remap_dupe.sh utility to run database command
for mapping in mappings:
    mapping = mapping.strip()
    mapping = mapping.replace('/handle/', "")
    mapping = mapping[:-1] # remove semicolon at end of line
    handles = mapping.split()
    print(f'{handles[0]} -> {handles[1]}')
    command = ["./remap_dupe.sh", handles[0], handles[1] ]
    with subprocess.Popen(command, stdout=subprocess.PIPE) as child:
        STREAM_DATA = child.communicate()[0]
        if child.returncode != 0:
            print("There was a problem remapping the handle.")
            sys.exit(1)